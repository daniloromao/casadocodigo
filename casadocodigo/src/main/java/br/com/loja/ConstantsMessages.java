package br.com.loja;

/**
 * Classe que possui constantes que apontam as chaves das mensagens do sistem.
 * @author danilo
 *
 */
public class ConstantsMessages {

	//Defaults
	public static final String FIELD_REQUIRED_DEFAULT = "field.required";
	public static final String TYPE_MISMATCH = "typeMismatch";
	
	//Products
	public static final String FIELD_REQUIRED_PRODUCT_TITLE = "field.required.product.title";
	public static final String TYPE_MISMATCH_PRODUCT_PAGES = "typeMismatch.product.pages";
	public static final String FIELD_REQUIRED_PRODUCT_SUMMARY_PATH = "field.required.product.summaryPath";
	
	//Java
	public static final String TYPE_MISMATCH_JAVA_LANG_INTEGER = "typeMismatch.java.lang.Integer";
	public static final String TYPE_MISMATCH_INT = "typeMismatch.int";
	
	//Negocio
	public static final String FILE_EXISTS = "file.exists";
}

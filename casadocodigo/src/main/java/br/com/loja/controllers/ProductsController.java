package br.com.loja.controllers;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.mysql.jdbc.StringUtils;

import br.com.loja.ConstantsMessages;
import br.com.loja.daos.ProductDAO;
import br.com.loja.models.BookType;
import br.com.loja.models.Product;
import br.com.loja.util.FileSaver;

@Controller
@Transactional
@RequestMapping("/produtos")
public class ProductsController {
	
	@Autowired
	protected ProductDAO productDAO;
	@Autowired
	protected FileSaver fileSaver;
	
	@RequestMapping(method = RequestMethod.POST, name="saveProduct")
	public ModelAndView save(MultipartFile summary, @Valid Product product, BindingResult bindingResult,
			RedirectAttributes redirectAttributes) {
		if(StringUtils.isNullOrEmpty(summary.getOriginalFilename())) {
			bindingResult.addError(new ObjectError("product.summaryPath", ConstantsMessages.FIELD_REQUIRED_DEFAULT));
		} else {
			String webPath = fileSaver.write("uploaded-images", summary);
			product.setSummaryPath(webPath);
		}
		
		if(bindingResult.hasErrors()) {
			ModelAndView andView = form(product);
			andView.addObject("product", product);
			return andView;
		}
		productDAO.save(product);
		redirectAttributes.addFlashAttribute("sucesso", "Produto cadastado com sucesso!");
		return new ModelAndView("redirect:produtos");
	}
	
	@RequestMapping(value = "/form", method = RequestMethod.GET)
	public ModelAndView form(Product product) {
		ModelAndView andView = new ModelAndView("products/form");
		andView.addObject("types", BookType.values());
		
		return andView;
	}
	
	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView list() {
		ModelAndView andView = new ModelAndView("products/list");
		andView.addObject("products", productDAO.list());
		
		return andView;
	}
}

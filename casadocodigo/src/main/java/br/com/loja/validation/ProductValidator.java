package br.com.loja.validation;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import br.com.loja.ConstantsMessages;
import br.com.loja.models.Product;

public class ProductValidator implements Validator{

	@Override
	public boolean supports(Class<?> clazz) {
		return Product.class.isAssignableFrom(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "title", ConstantsMessages.FIELD_REQUIRED_DEFAULT);
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "description", ConstantsMessages.FIELD_REQUIRED_DEFAULT);
		
		Product product = (Product) target;
		
		if(product.getPages() <= 0) {
			errors.rejectValue("pages", ConstantsMessages.FIELD_REQUIRED_DEFAULT);
		}
	}

}

<!DOCTYPE html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<html>
	<head>
		<meta charset="UTF-8">
		<title>Cadastro de Produto</title>
	</head>
	<body>
		<c:url value="/produtos" var="urlProdutos"></c:url>
		<form:form method="POST" action="${urlProdutos}" commandName="product" enctype="multipart/form-data">
			<div>
				<label for="title">Titulo</label>
				<form:input path="title" type="text" />
				<form:errors path="title" />
			</div>
			<div>
				<label for="description">Descri��o</label>
				<form:textarea path="description" rows="10" cols="20" id="description"/>
				<form:errors path="description" />
			</div>
			<div>
				<label for="pages">N�mero de P�ginas</label>
				<form:input path="pages" id="pages" />
				<form:errors path="pages"/>
			</div>
			<div>
				<label for="releaseDate">Data de Lan�amento</label>
				<form:input path="releaseDate" type="date" id="releaseDate"/>
				<form:errors path="releaseDate" />
			</div>
			<div>
				<c:forEach items="${types}" var="bookType" varStatus="status">
					<div>
						<label for="price_${bookType}">${bookType}</label>
						<form:input path="prices[${status.index}].value"/>
						<form:input path="prices[${status.index}].bookType" type="hidden" value="${bookType}"/>
						<form:errors path="prices[${status.index}].value"/>
					</div>
				</c:forEach>
			</div>
			<div>
				<label for="summary">Sumario</label>
				<input name="summary" type="file"/>
				<form:errors path="summaryPath" />
			</div>
			<div>
				<input type="submit" value="enviar"/>
			</div>
		</form:form>
	</body>
</html>
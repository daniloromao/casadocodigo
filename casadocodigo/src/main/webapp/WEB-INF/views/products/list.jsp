<!DOCTYPE html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset='UTF-8'" >
		<title>Livros da Casa do Codigo</title>
	</head>
	<body>
		${sucesso}
		<table>
			<tr>
				<td>T�tulo</td>
				<td>Valores</td>
				<td>Data de Lan�amento</td>
			</tr>
			<c:forEach items="${products}" var="product">
				<tr>
					<td>${product.title}</td>
					<td>
						<c:forEach items="${product.prices}" var="price">
							[${price.value} - ${price.bookType}]
						</c:forEach>
					</td>
					<td>${product.releaseDate.getTime()}</td>
				</tr>
			</c:forEach>
		</table>
	</body>
</html>